package projetoimc;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.FileNotFoundException;

public class ProjetoIMC {

    public static void main(String[] args) {
        
        BufferedReader csv = null;
        String linha;
        String separador = ";";
        boolean primeiraLn = true;
        try {
            csv = new BufferedReader(new InputStreamReader(new FileInputStream("dataset.csv"), "ISO-8859-1")); //Leitura do arquivo com a codificação ISO-8859-1
            while ((linha = csv.readLine()) != null)
            {
                if(primeiraLn == true) //Pula a primeira linha
                {
                    primeiraLn = false;
                }else //Processo de separação de campos e linhas
                { 
                    String[] pos = linha.split(separador);
                    PessoaIMC atual = new PessoaIMC(pos[0], pos[1]);
                    if (pos.length == 4) //Condição para a existência dos campos na linha
                    {
                        atual.calculoIMC(Float.parseFloat(pos[2].replaceAll(",",".")), Float.parseFloat(pos[3].replaceAll(",",".")));
                    }
                    atual.saidaResultado(); //Impressão dos dados
                }
            }
        }catch (UnsupportedEncodingException e){ //Catch de exceções
            System.out.println("Unsupported Encoding Exception: \n"+e.getMessage());
        } catch(FileNotFoundException e){
            System.out.println("File Not Found Exception: \n"+e.getMessage());
        } catch (IOException e) {
            System.out.println("IO Exception: \n" +e.getMessage());
        }finally
        {
            if (csv != null){
                try {
                    csv.close();
                } catch(IOException e){
                    System.out.println("IO Exception: \n" +e.getMessage());
                }
            }
        }
    }
}
