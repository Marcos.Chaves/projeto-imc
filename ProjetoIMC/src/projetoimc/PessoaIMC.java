package projetoimc;
import java.text.DecimalFormat;

public class PessoaIMC 
{
    private String nome;
    private String sobrenome;
    private float valorIMC = 0; //O valor 0 vai ser exibido para qualquer erro nos dados recebidos
    DecimalFormat formato = new DecimalFormat("0.00");
    
    public PessoaIMC(String nome, String sobrenome) //Método construtor, faz as strings da classe receberem os valores formatados
    {
        this.nome = nome.trim().replaceAll(" +", " ");
        this.sobrenome = sobrenome.trim().replaceAll(" +", " ");
    }
    public void calculoIMC(float peso, float altura) //Método de cálculo do IMC
    {
        valorIMC = peso/(altura * altura);
    }
    public void saidaResultado () //Método de impressão da linha
    {
        String valorString;
        nome = nome.toUpperCase();
        sobrenome = sobrenome.toUpperCase();
        valorString = formato.format(valorIMC);
            
        System.out.println(nome + " " + sobrenome  + " " + valorString);
    }
}
